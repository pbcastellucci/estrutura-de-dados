#include <iostream>
#include <list>
#include <vector>
#include <algorithm>

class Edge{
private:
    int u, v;
public:
    Edge(int _u, int _v){
	u = _u;
	v = _v;
    }
    std::pair<int, int> endpoints() const{
	return std::make_pair(u, v);
    }
    int opposite(int i) const{
        if (i == u) {
	    return v;
	}
	return u;
    }
    bool operator==(const Edge& rhs) const{
	auto e = rhs.endpoints();
	return u == e.first and v == e.second;
    }
};


class Graph{
private:
    std::vector<std::list<Edge>> vertices;
    
public:
    Graph(int n_vertices){
	vertices.resize(n_vertices);
    }

    unsigned n_vertices() const {
	return vertices.size();
    }
    
    void add_edge(int u, int v){
	Edge e(u, v);
	auto elem = std::find(vertices[u].begin(), vertices[u].end(), e);
	if (elem == vertices[u].end()) {
	    vertices[u].push_back(e);
	    vertices[v].push_back(Edge(v, u));
	}
	else {
	    std::cout << "Warning... edge already in Graph (" << u << ',' << v << ")\n";
	}
    }

    std::list<Edge> incident_edges(int u){
	return vertices[u];
    }
    
    friend std::ostream& operator<<(std::ostream& out, const Graph& g){
	for (auto i = 0u; i < g.vertices.size(); ++i) {
	    out << i << ": ";
	    for (auto& it: g.vertices[i]) {
		auto elem = it.endpoints();
		out << '(' << elem.first << ", " << elem.second << "); ";
	    }
	    out << '\n';
	}
	return out;
    }
    
};


void DFS(Graph& g, int v, std::vector<bool>& discovered){
    std::cout << "Visiting node " << v << '\n';
    discovered[v] = true;
    for (auto e: g.incident_edges(v)) {
	int u = e.opposite(v);
	if (discovered[u] == false) {
	    DFS(g, u, discovered);
	}
    }
}


int main()
{
    Graph g(5);
    g.add_edge(0, 1);
    g.add_edge(0, 2);
    g.add_edge(0, 4);
    g.add_edge(1, 3);
    g.add_edge(3, 4);
    std::vector<bool> discovered(g.n_vertices(), false);
    DFS(g, 0, discovered);
    std::cout << g; 
    return 0;
}

