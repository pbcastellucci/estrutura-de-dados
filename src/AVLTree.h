#include <iostream>
#include <vector>

class Node{
public:
    Node();
    Node(const int& val):
	_elem(val){}
    
    Node(const int& val, Node* left, Node* right, Node* parent):
	_elem(val),
	_left(left),
	_right(right),
	_parent(nullptr){}
    Node* left() const {return _left; };    
    Node* right() const {return _right; };
    int get() const {return _elem; };
    unsigned size() const {return _sz; };
    int get_bf() const {return _bf;} ;
private:
    int _elem;
    Node* _left = nullptr;
    Node* _right = nullptr;
    Node* _parent = nullptr;
    unsigned _sz = 0;
    unsigned _bf = 0;
    
    friend class AVLTree;
};


class AVLTree{
public:
    void insert(const int elem);
    Node* find(const int elem) const;
    void remove(int elem);
    void print_inorder() const;
    bool empty() const;
    AVLTree() = default;
    ~AVLTree();
private:
    Node* root = nullptr;
    Node* find_recursive(Node* pivot, const int& elem) const;
    void delete_tree(Node* root);
    void print_inorder(Node* pivot) const;
    Node* insert_node(Node* pivot, const int& elem);
    void rotate_left(Node& x, Node& y);
    void rotate_right(Node& x, Node& y);
};



