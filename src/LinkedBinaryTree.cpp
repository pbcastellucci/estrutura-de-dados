#include <iostream>
#include <list>
#include <vector>

// Warning This should be changed to a template!
#define data_type int

class Node{

private:
    data_type _key;
    Node* _left;
    Node* _right;
    Node* _parent;
public:
    // We are using the list initializer of the parameters.
    Node(data_type _k):
	_key(_k), _left(nullptr), _right(nullptr) {}
    Node(data_type _k, Node* __left, Node* __right):
	_key(_k), _left(__left), _right(__right) {}

    data_type get_key() const {return _key;};
    
    Node* left() const {
	return _left;
    };
    
    Node* right() const {
	return _right;
    };

    // Friend classes can access private members
    friend class LinkedBinaryTree;
};


class LinkedBinaryTree{
    
public:    
    LinkedBinaryTree(){
	root = nullptr;
    }

    void insert(data_type key){
	Node* new_node = _insert_leaf(root, new Node(key));
	if (is_empty()) {
	    root = new_node;
	}
    }

    bool is_empty(){
	return root == nullptr;
    }

    Node* find(data_type key){
	return _find(root, key);
    }

    void remove(data_type key){
	Node* pivot = find(key);
	if (pivot) {
	    _remove(pivot);	      
	}
    }

    Node* min() const{
	return _min(root);
    }

    Node* max() const{
	return _max(root);
    }
    
    std::list<data_type> get_preorder(){
	std::list<data_type> nodes;
	_get_preorder(root, &nodes);
	return nodes;
    }
    
    std::list<data_type> get_inorder(){
	std::list<data_type> nodes;
	_get_inorder(root, &nodes);
	return nodes;
    }

    std::list<data_type> get_postorder(){
	std::list<data_type> nodes;
	_get_postorder(root, &nodes);
	return nodes;
    }
	
    ~LinkedBinaryTree(){
	_delete_tree(root);
    }
    
private:
    Node* root;

    Node* _find(Node* pivot, data_type x){
	if (pivot == nullptr) {
	    return pivot;
	}
	if (x < pivot->get_key()) {
	    return _find(pivot->left(), x);
	}
	if (x > pivot->get_key()) {
	    return _find(pivot->right(), x);
	}
	return pivot;
    }

    Node* _insert_leaf(Node* pivot, Node* new_node){
	if (pivot == nullptr) {
	    return new_node;
	}
	if (new_node->get_key() <= pivot->get_key()) {
	    pivot->_left = _insert_leaf(pivot->_left, new_node);
	    pivot->_left->_parent = pivot;
	}
	else {
	    pivot->_right = _insert_leaf(pivot->_right, new_node);
	    pivot->_right->_parent = pivot;
	}
	return pivot;
    }

    Node* _min(Node* pivot) const{
	if (pivot->left()) {
	    return _min(pivot->left());
	}
	return pivot;
    }

    Node* _max(Node* pivot) const{
	while (pivot->right()) {
	    pivot = pivot->right();
	}
	return pivot;
    }	  

    void _remove(Node* pivot){
	if (pivot->left() and pivot->right()) {
	    Node* next = _max(pivot->left());
	    pivot->_key = next->_key;
	    _remove(next);
	}
	else if (pivot->left()) {
	    Node* to_delete = pivot;
	    if (pivot->_parent) {
		pivot->_parent->_left = pivot->_left;
		pivot->_left->_parent = pivot->_parent;
	    }
	    else {		  
		root = pivot->left();
		pivot->_left->_parent = nullptr;
	    }
	    delete to_delete;
	}
	else if (pivot->right()) {
	    Node* to_delete = pivot;
	    if (pivot->_parent) {
		pivot->_parent->_right = pivot->_right;
		pivot->_right->_parent = pivot->_parent;
	    }
	    else {		  
		root = pivot->right();
		pivot->_right->_parent = nullptr;
	    }
	    delete to_delete;
	}
	else {
	    Node* to_delete = pivot;
	    Node* p = pivot->_parent;
	    if (p->left() == pivot) {
		p->_left = nullptr;
	    }
	    else {
		p->_right = nullptr;
	    }
	    delete to_delete;
	}
    }
      
      
    void _delete_tree(Node* pivot){
	// Using post order to delete the tree
	if (pivot) {
	    _delete_tree(pivot->left());
	    _delete_tree(pivot->right());
	    delete pivot;
	}
    }
	  
    void _get_preorder(Node* p, std::list<data_type>* nodes){
	if (p) {
	    nodes->push_back(p->get_key());
	    _get_preorder(p->_left, nodes);
	    _get_preorder(p->_right, nodes);
	}
    }

    void _get_inorder(Node* p, std::list<data_type>* nodes){
	if (p) {
	    _get_inorder(p->_left, nodes);
	    nodes->push_back(p->get_key());
	    _get_inorder(p->_right, nodes);
	}
    }

    void _get_postorder(Node* p, std::list<data_type>* nodes){
	if (p) {
	    _get_postorder(p->_left, nodes);
	    _get_postorder(p->_right, nodes);
	    nodes->push_back(p->get_key());
	}
    }    
};


int main(void)
{
    std::vector<int> items = {50, 20, 75, 10, 40, 60, 80, 15, 55, 65, 100, 120};
    LinkedBinaryTree T;
    for (const auto& i: items) {
	T.insert(i);
    }
    
    std::cout << "Pre-order:\n";
    for (const auto& x : T.get_preorder()) {
	std::cout << x << ' ';
    }
    std::cout << "\nIn-order:\n";
    for (const auto& x : T.get_inorder()) {
	std::cout << x << ' ';
    }

    std::cout << "\nPost-order:\n";
    for (const auto& x : T.get_postorder()) {
	std::cout << x << ' ';
    }

    // std::cout << "\nMax = " << T.max()->get_key();
    // std::cout << "\nMin = " << T.min()->get_key();

    T.remove(65);
    std::cout << "\nPre-order:\n";
    for (const auto& x : T.get_preorder()) {
	std::cout << x << ' ';
    }
    std::cout << "\nIn-order:\n";
    for (const auto& x : T.get_inorder()) {
	std::cout << x << ' ';
    }

    std::cout << "\nPost-order:\n";
    for (const auto& x : T.get_postorder()) {
	std::cout << x << ' ';
    }

    T.remove(80);
    std::cout << "\nPre-order:\n";
    for (const auto& x : T.get_preorder()) {
	std::cout << x << ' ';
    }
    std::cout << "\nIn-order:\n";
    for (const auto& x : T.get_inorder()) {
	std::cout << x << ' ';
    }

    std::cout << "\nPost-order:\n";
    for (const auto& x : T.get_postorder()) {
	std::cout << x << ' ';
    }

    T.remove(75);
    std::cout << "\nPre-order:\n";
    for (const auto& x : T.get_preorder()) {
	std::cout << x << ' ';
    }
    std::cout << "\nIn-order:\n";
    for (const auto& x : T.get_inorder()) {
	std::cout << x << ' ';
    }

    std::cout << "\nPost-order:\n";
    for (const auto& x : T.get_postorder()) {
	std::cout << x << ' ';
    }
    std::cout << '\n';
    return 0;
}
