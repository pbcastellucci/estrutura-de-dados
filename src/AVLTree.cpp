#include <iostream>
#include <list>
#include <vector>

// Warning This should be changed to a template!
#define data_type int

class Node{

private:
    data_type _key;
    Node* _left;
    Node* _right;
    int _bf;
public:
    // We are using the list initializer of the parameters.
    Node(data_type _k):
	_key(_k), _left(nullptr), _right(nullptr), _bf(0) {}
    Node(data_type _k, Node* __left, Node* __right):
	_key(_k), _left(__left), _right(__right), _bf(0) {}

    data_type get_key() const {return _key;};
    
    Node* left() const {
	return _left;
    };
    
    Node* right() const {
	return _right;
    };

    int get_bf() const{
	return _bf;
    }
    
    // Friend classes can access private members
    friend class AVLTree;
};


class AVLTree{
    
public:    
    AVLTree(){
	root = nullptr;
    }

    void insert(data_type key){
	bool new_height = false;
	std::cout << "Inserting " << key << '\n';
	root = _insert_leaf(root, new Node(key), &new_height);
    }

    bool is_empty(){
	return root == nullptr;
    }

    Node* find(data_type key){
	Node* pivot = _find(root, key);
	if (pivot) {
	    return pivot;
	}
	return nullptr;
    }

    void remove(data_type key){
	Node* pivot = find(key);
	if (pivot) {
	    _remove(pivot);	      
	}
    }

    Node* min() const{
	return _min(root);
    }

    Node* max() const{
	return _max(root);
    }
    
    std::list<data_type> get_preorder(){
	std::list<data_type> nodes;
	_get_preorder(root, &nodes);
	return nodes;
    }
    
    std::list<data_type> get_inorder(){
	std::list<data_type> nodes;
	_get_inorder(root, &nodes);
	return nodes;
    }

    std::list<data_type> get_postorder(){
	std::list<data_type> nodes;
	_get_postorder(root, &nodes);
	return nodes;
    }
	
    ~AVLTree(){
	_delete_tree(root);
    }
    
private:
    Node* root;

    Node* _find(Node* pivot, data_type x){
	if (pivot == nullptr) {
	    return pivot;
	}
	if (x < pivot->get_key()) {
	    return _find(pivot->left(), x);
	}
	if (x > pivot->get_key()) {
	    return _find(pivot->right(), x);
	}
	return pivot;
    }

    Node* _insert_leaf(Node* pivot, Node* new_node, bool* new_height){
	if (pivot == nullptr) {
	    *new_height = true;
	    return new_node;
	}
	if (new_node->get_key() <= pivot->get_key()) {
	    pivot->_left = _insert_leaf(pivot->_left, new_node, new_height);
	    if (*new_height == true) {
		if (pivot->get_bf() == +1) {
		    pivot->_bf = 0;
		    *new_height = false;
		}
		else if (pivot->get_bf() == 0) {
		    pivot->_bf = -1;
		    *new_height = true;
		}
		else if (pivot->get_bf() == -1) {
		    if (pivot->_left->get_bf() == -1) {
			pivot = rotate_right(pivot);
			pivot->_right->_bf = 0;
		    }
		    else{
			pivot->_left = rotate_left(pivot->left());
			pivot = rotate_right(pivot);
			if (pivot->get_bf() == +1) {
			    pivot->_left->_bf = -1;
			    pivot->_right->_bf = 0;
			}
			else if (pivot->get_bf() == -1) {
			    pivot->_left->_bf = 0;
			    pivot->_right->_bf = 1;
			}
			else {
			    pivot->_left->_bf = pivot->_right->_bf = 0;
			}			
		    }
		    pivot->_bf = 0;
		    *new_height = false;
		}
	    }
	}
	else {
	    pivot->_right = _insert_leaf(pivot->_right, new_node, new_height);
	    if (*new_height == true) {
		if (pivot->get_bf() == -1) {
		    pivot->_bf = 0;
		    *new_height = false;
		}
		else if (pivot->get_bf() == 0) {
		    pivot->_bf = 1;
		    *new_height = true;
		}
		else if (pivot->get_bf() == 1) {
		    if (pivot->_right->get_bf() == 1) {
			pivot = rotate_left(pivot);
			pivot->_left->_bf = 0;
		    }
		    else{
			pivot->_right = rotate_right(pivot->right());
			pivot = rotate_left(pivot);
			if (pivot->get_bf() == +1) {
			    pivot->_left->_bf = -1;
			    pivot->_right->_bf = 0;
			}
			else if (pivot->get_bf() == -1) {
			    pivot->_left->_bf = 0;
			    pivot->_right->_bf = 1;
			}
			else {
			    pivot->_left->_bf = pivot->_right->_bf = 0;
			}
		    }
		    pivot->_bf = 0;
		    *new_height = false;
		}
	    }
	}
	return pivot;
    }

    Node* _min(Node* pivot) const{
	if (pivot->left()) {
	    return _min(pivot->left());
	}
	return pivot;
    }

    Node* _max(Node* pivot) const{
	while (pivot->right()) {
	    pivot = pivot->right();
	}
	return pivot;
    }	  

    void _remove(Node* pivot){
    }     
    
    Node* rotate_left(Node* pivot){
	Node* aux = pivot->right();
	pivot->_right = aux->left();
	aux->_left = pivot;
	return aux;	    
    }

    Node* rotate_right(Node* pivot){
	Node* aux = pivot->left();
	pivot->_left = aux->right();
	aux->_right = pivot;
	return aux;
    }
    
    void _delete_tree(Node* pivot){
	// Using post order to delete the tree
	if (pivot) {
	    _delete_tree(pivot->left());
	    _delete_tree(pivot->right());
	    delete pivot;
	}
    }
	  
    void _get_preorder(Node* p, std::list<data_type>* nodes){
	if (p) {
	    nodes->push_back(p->get_key());
	    _get_preorder(p->_left, nodes);
	    _get_preorder(p->_right, nodes);
	}
    }

    void _get_inorder(Node* p, std::list<data_type>* nodes){
	if (p) {
	    _get_inorder(p->_left, nodes);
	    nodes->push_back(p->get_key());
	    _get_inorder(p->_right, nodes);
	}
    }

    void _get_postorder(Node* p, std::list<data_type>* nodes){
	if (p) {
	    _get_postorder(p->_left, nodes);
	    _get_postorder(p->_right, nodes);
	    nodes->push_back(p->get_key());
	}
    }    
};


int main(void)
{
    //std::vector<int> items = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    AVLTree T;
    T.insert(499);
    T.insert(171);
    T.insert(57);
    T.insert(392);
    T.insert(399);
    T.insert(500);
    T.insert(398);
    T.insert(397);
    T.insert(395);
    T.insert(393);
    std::cout << "Pre-order:\n";
    for (const auto& x : T.get_preorder()) {
	std::cout << x << ' ';
    }
    std::cout << "\nIn-order:\n";
    for (const auto& x : T.get_inorder()) {
	std::cout << x << ' ';
    }

    std::cout << "\nPost-order:\n";
    for (const auto& x : T.get_postorder()) {
	std::cout << x << ' ';
    }

    std::cout << '\n';
    return 0;
}
