#+TITLE: Árvores binárias
#+AUTHOR: Pedro Belin Castellucci
#+LANGUAGE: br
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="myorgstyle.css"/>


* Árvores binárias

Uma árvore binária é uma árvore em que cada nó:
- Possui um filho da esquerda e um da direita, sendo cada um deles uma subárvore.
- ou é uma árvore vazia.

Note que essa definição impede a união de caminhos e o aparecimento de ciclos na estrutura da
árvore. 

#+BEGIN_SRC dot :file images/arvore_nao_arvore01.png :cmdline -Kdot -Tpng :eval no-export :html-html5-fancy
  digraph g {
  node [];
  node0[label = "3 "];
  node1[label = "1 "];
  node2[label = "4 "];
  node3[label = "9 "];
  node4[label = "6 "];
  node5[label = "5 "];
  node6[label = "3 "];
  node7[label = "1 "];
  node8[label = "5 "];

  node0 -> node1;
  node0 -> node2;
  node1 -> node3[color=red];
  node2 -> node3[color=red];

  node4 -> node5[color=red];
  node4 -> node5[color=red];

  node6 -> node7;
  node7 -> node8;
  node8 -> node6[color=red];
  }
#+END_SRC

#+CAPTION: Exemplos de estruturas que não são árvores. As arestas em vermelho indicam violação da definição de árvore.
#+RESULTS:
[[file:images/arvore_nao_arvore01.png]]


Dois exemplos de árvores binárias são dados a seguir. Note que árvores diferentes podem ser geradas
a partir do mesmo conjunto de nós.

#+BEGIN_SRC dot :file images/arvore_example01.png :cmdline -Kdot -Tpng :eval no-export :html-html5-fancy
 digraph g {
 node [];
 node0[label = "3 "];
 node1[label = "1 "];
 node2[label = "4 "];
 node3[label = "9 "];
 node4[label = "6 "];
 node5[label = "5 "];
 node6[label = "3 "];
 node7[label = "1 "];
 node8[label = "5 "];
 node0 -> node4;
 node0 -> node1;
 node1 -> node2;
 node1 -> node3;
 node2 -> node8;
 node2 -> node7;
 node4 -> node6;
 node4 -> node5;
 }
#+END_SRC

#+CAPTION: Árvore com altura 4.
#+RESULTS:
[[file:images/arvore_example01.png]]


#+BEGIN_SRC dot :file images/arvore_example02.png :cmdline -Kdot -Tpng :eval no-export :html-html5-fancy
 digraph g {
 rankdir=LR;
 node [];
 node0[label = "3 "];
 node1[label = "1 "];
 node2[label = "4 "];
 node3[label = "1 "];
 node4[label = "5 "];
 node5[label = "9 "];
 node6[label = "2 "];
 node7[label = "6 "];
 node8[label = "5 "];
 node0 -> node1;
 node1 -> node2;
 node2 -> node3;
 node3 -> node4;
 node4 -> node5;
 node5 -> node6;
 node6 -> node7;
 node7 -> node8;
 }
#+END_SRC

#+CAPTION: Árvore com altura 8.
#+RESULTS:
[[file:images/arvore_example02.png]]

Note que a altura das árvores resultantes são diferentes. Além disso, a altura de uma árvore com o
mesmo conjunto de elementos pode variar bastante.

#+begin_exercise
Determine as alturas máxima e mínima de uma árvore binária com $n$ elementos.
#+end_exercise

 * A altura máxima é $h = n - 1$, que corresponde ao caso "degenerado"
   em que a árvore é equivalente a uma lista.
 * A altura mínima pode ser estimada observando o número máximo de nós
   em cada nível de uma árvore $n = (1 + 2 + 4 + \ldots + \frac{n}{2})$,
   que trata-se de uma Progressão Geométrica de razão dois. A soma da
   PG pode ser calculada como $n = \frac{1(2^{h} - 1)}{2 - 1} =
   2^{h} - 1$; resolvendo para $n$ obtemos que $h = O(log(n))$

#+begin_exercise
Determine o maior número de nós em um nível $d$ de uma árvore binária.
#+end_exercise

#+begin_highlight
Uma árvore binária é chamada de própria (ou cheia) se cada nó possui
zero ou dois filhos.
#+end_highlight

** Percursos em árvores binárias

Da mesma forma que em [[file:arvores.org][Árvores]] gerais, árvores binárias também podem
ser exploradas utilizando:

1. Percurso em pré-ordem
2. Percurso em pós-ordem
3. Percurso em largura

Mas também é possível utilizar percurso em ordem simétrica (ou
percurso em-ordem). Para todos os percursos, considere como exemplo a
árvore a seguir. 

#+BEGIN_SRC dot :file images/arvore_binaria_example01.png :cmdline -Kdot -Tpng :eval no-export :html-html5-fancy
  digraph g {

  A -> B;
  A -> C;
  B -> D;
  B -> E;
  C -> F;
  C -> G;
  G -> H;
  G -> I;
  H -> J;
  H -> K;

  }
#+END_SRC

#+RESULTS:
[[file:images/arvore_binaria_example01.png]]


*** Percurso em pré-ordem

O percurso em pré-ordem visita a raiz, em seguida as subárvores da
esquerda e da direita em pré-ordem, respectivamente.

#+begin_src python
  def preorder(root):
      if root is not None:
	  visit(root)
	  preorder(left(root))
	  preorder(right(root))
#+end_src

Para a árvore de exemplo, os nós seriam visitados na seguinte ordem:

- A, B, D, E, C, F, G, H, J, K, I

*** Percurso em-ordem

O percurso em-ordem (ordem simétrica) percorre a subárvore da esquerda
em-ordem, visita a raiz e, em seguida, percorre a subárvore da direita
em-ordem, respectivamente.

#+begin_src python
  def inorder(root):
      if root is not None:
	  inorder(left(root))
	  visit(root)
	  inorder(right(root))
#+end_src

Para a árvore de exemplo, os nós seriam visitados na seguinte ordem:

- D, B, E, A, F, C, J, H, K, G, I


*** Percurso em pós-ordem

O percurso em pós-ordem percorre a subárvore da esquerda em pós-ordem,
da direita em pós-ordem e, então, visita a raiz.
#+begin_src python
  def postorder(root):
      if root is not None:
	  postorder(left(root))
	  postorder(right(root))
	  visit(root)
#+end_src

Para a árvore de exemplo, os nós seriam visitados na seguinte ordem:

- D, E, B, F, J, K, H, I, G, C, A


#+begin_exercise
Apresente um pseudo-código para o percurso em largura em uma árvore binária.
#+end_exercise

As operações utilizadas /left()/ e /right()/ utilizadas nos algoritmos
de percurso dependem da implementação da estrutura de
dados. Tipicamente, duas implementações são exploradas: uma em vetor
outra em estruturas encadeadas.


* Representação de árvore em vetor

#+begin_highlight
Uma árvore binária completa é uma árvore binária em que todos os
níveis, com possível exceção do último, estão completamente
cheios e todos os nós estão o mais à esquerda possível.
#+end_highlight

Uma implementação de árvore em vetor pode ser construída da seguinte
forma.
- A raiz é armazenada na posição zero do vetor.
- Posição $2i + 1$: armazena o nó filho da esquerda do nó na posição $i$.
- Posição $2i + 2$: armazena o nó filho da direita do nó na posição $i$.

Dessa forma, a árvore


#+BEGIN_SRC dot :file images/arvore_em_vetor01.png :cmdline -Kdot -Tpng :eval no-export :html-html5-fancy
  digraph g {

  A -> B;
  A -> C;
  B -> D;
  B -> E;
  C -> F;
  C -> G;
  D -> H;
  D -> I;    
  }
#+END_SRC

#+RESULTS:
[[file:images/arvore_em_vetor01.png]]

pode ser armazenada no vetor

#+begin_src dot dot :file images/arvore_em_vetor_representacao.png :cmdline -Kdot -Tpng :eval no-export :html-html5-fancy
  digraph so
  {
      rankdir="LR";
      Array [ shape = record, label = "{ A | B | C | D | E | F | G | H | I }"] ;
      notes [shape=record, width=1, color=white, label = "{ 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 }" ];
      nodesep = .0;

  }
#+end_src

#+RESULTS:
[[file:images/arvore_em_vetor_representacao.png]]

Por consequência,

#+begin_highlight
O pai do nó na posição $i$ está na posição $\lfloor \frac{i-1}{2} \rfloor$.
#+end_highlight

A implementação em vetor é simples e pode ser generalizada para
árvores M-árias. No entanto, caso a árvore não seja completa, pode
haver grande desperdício de memória.

Essa implementação pode ser utilizada em [[file:Heap.org][Filas de prioridade com /heap/]].


* Representação de árvore em estrutura encadeada

Em termos de estrutura de dados, uma árvore pode ser representada de forma que cada nó é uma estrutura com:
- Um conteúdo,
- Um apontador para o filho da esquerda,
- Um apontador para o filho da direita,
- Um apontador para o pai (opcional).

#+BEGIN_SRC dot :file images/arvore_estrutura_ligada.png :cmdline -Kdot -Tpng :eval no-export :html-html5-fancy
  digraph g {
  node [shape = record, height=.1];
  edge [headclip=false, tailclip=false];

  node0[label = "<f0> |<f1> G|<f2> "];
  node1[label = "<f0> |<f1> E|<f2> "];
  node2[label = "<f0> |<f1> B|<f2> "];
  node3[label = "<f0> |<f1> F|<f2> "];
  node4[label = "<f0> |<f1> R|<f2> "];
  node5[label = "<f0> |<f1> H|<f2> "];
  node6[label = "<f0> |<f1> Y|<f2> "];
  node7[label = "<f0> |<f1> A|<f2> "];
  node8[label = "<f0> |<f1> C|<f2> "];
  root[color=white];
  "node0":f2:c -> "node4":f1;
  "node0":f0:c -> "node1":f1;
  "node1":f0:c -> "node2":f1;
  "node1":f2:c -> "node3":f1;
  "node2":f2:c -> "node8":f1;
  "node2":f0:c -> "node7":f1;
  "node4":f2:c -> "node6":f1;
  "node4":f0:c -> "node5":f1;
  root -> node0[headclip=true, tailclip=true];
  }
#+END_SRC

#+RESULTS:
[[file:images/arvore_estrutura_ligada.png]]

Relacionado à implementação pode-se definir o seguinte registro.
#+BEGIN_SRC C
  struct Node
  {
      T conteudo;
      struct Node *parent; // opcional
      struct Node *left;
      struct Node *right;
  };
#+END_SRC

- Lembrando que uma subárvore de um nó $x$ é definida como $x$ e seu conjunto de nós descendentes,
  isto é, todos os nós para os quais existe um caminho a partir de $x$. Também, pode-se dizer que
  trata-se de árvore enraizada em $x$.

** Operações elementares

No caso das estruturas lineares estudadas anteriormente (listas, pilhas e filas) dentre as operações
elementares estavam inserção e remoção de elementos. No caso de árvore, tais operações não são,
necessariamente, elementares devido à presença de uma segunda ligação em cada nó. Por exemplo, a
remoção de um nó que não é folha, pode ser necessário reorganizar diversas ligações para manter uma
estrutura de árvore. 

No caso de árvores, algumas operações elementares são: 
- Inserir um nó folha.
- Remover um nó folha. 
- Combinar duas árvores $L$ e $R$, criando uma nova raiz cujas subárvores são $L$ e $R$.

Tipicamente, a implementação das demais operações depende do
problema a ser resolvido. Ou então, combinada com alguma propriedade
adicional desejada para a árvore, como é o caso de [[file:arvore_binaria_busca.org][Árvores Binárias de Busca]].

* Referências

- Fortemente baseado no material do Prof. Dr. Mário Felice (http://www2.dc.ufscar.br/~mario/#teaching)
