#+TITLE: Pilhas
#+AUTHOR: Pedro Belin Castellucci
#+LANGUAGE: br
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="myorgstyle.css"/>

* Introdução

#+begin_quote
Uma pilha (/stack/) é uma estrutura de dados sequencial e dinâmica na qual inserção e
remoção ocorrem na mesma extremidade da sequência, chamada
de topo da Pilha.
#+end_quote

Uma pilha segue a política LIFO (*last-in, first-out*), isto é, o
elemento a ser removido é o que foi inserido mais recentemente.

Note que uma pilha pode ser considerada uma especialização de uma
lista. Em outras palavras, uma pilha é uma lista que só permite
inserções e remoções em uma extremidades.

** Algumas operações em pilhas

Algumas operações típicas do tipo abstrato de dados pilha são as
seguinte.

 * /push/: inserir um elemento na pilha;
 * /pop/: remover um elemento na pilha;
 * /top/: retornar o elemento no topo da pilha (sem removê-lo).
 * verificar o número de elemento na pilha.
 * verificar se a pilha está vazia.

Outras operações são possíveis, veja por exemplo, [[https://en.cppreference.com/w/cpp/container/stack][o contêiner /stack/
da STL do C++]].

Duas possíveis implementações para a estrutura de dados pilha são:
utilizando um vetor ou utilizando uma estrutura encadeada.

* Pilha em vetor

Para uma implementação em vetor é necessário:

 * Um vetor para armazenar os elementos da pilha; e
 * Uma referência para a posição da próxima inserção (o topo da
   pilha).

#+begin_src dot dot :file images/pilha-vetor.png :cmdline -Kdot -Tpng :eval no-export :html-html5-fancy
  digraph so
  {
      rankdir="LR";
      Array [shape=record, label = "{ A | B | C | -- | -- | -- | -- }"] ;
      notes [shape=record, width=1, color=white, label = "{ 0 | 1 | 2 | 3 | 4 | 5 | 6 }" ];
      nodesep = .0;
      label="topo = 3";
  }
#+end_src

#+RESULTS:
[[file:images/pilha-vetor.png]]
 
Note que o =topo= faz referência a uma célula que *não* possui
elementos da pilha, pois é a posição da próxima inserção.

#+begin_highlight
Lembrando que nesse tipo de implementação os elementos são armazenadas
sequencialmente em memória.
#+end_highlight

** Operações

*** Inserção (/push/)

Supondo que há espaço para inserção, basta realizar atribuir o
elemento a ser inserido ao topo da pilha e atualizar a variável
=topo=. Em C++, uma possibilidade é utilizar um comando como o
seguinte.

#+begin_src C++
  pilha[topo++] = x;  // Para inserção do elemento x.
#+end_src

Portanto, o número de operações realizadas é $O(1)$. Note que, caso
não há espaço suficiente na pilha, é necessário realizar uma nova
alocação e o custo se torna $O(n)$, sendo $n$ o tamanho do vetor
alocado para a pilha. 

*** Remoção (/pop/)

Para a remoção, basta atualizar o valor da variável =topo=.

#+begin_src C++
  if (not empty()) {
      topo--;
  }
#+end_src

A função =empty()= indica se a pilha está vazia (como isso pode ser
realizado?) e tem custo $O(1)$. Portanto, a remoção possui custo $O(1)$.

*** Outras operações

 * =top=: basta retornar a posição $topo-1$ (o que fazer caso a pilha
   esteja vazia?)
 * =size=: basta retorna o valor da variável =topo=.
 * =empty=: basta compara o =topo= com a primeira posição do vetor.

 #+begin_exercise
 Certifique-se de que as operações =top=, =size= e =empty= possui
 complexidade $O(1)$.
 #+end_exercise


* Pilha em estrutura encadeda
   
Uma pilha em estrutura encadeada utiliza raciocínios semelhantes à
implementação de [[file:listas_encadeadas.org][lista encadeada]]. 

#+begin_src dot :file images/pilha_linked.png :cmdline -Kdot -Tpng :eval no-export :html-html5-fancy
  digraph foo {
	  rankdir=LR;
	  node [shape=record];
	  edge [tailclip=false];
	  header [label="{ <data>  | <ref> }", style=filled, fillcolor="#666666"];
	  a [label="{ <data> A | <ref> }"];
	  b [label="{ <data> B | <ref> }"];
	  c [label="{ <data> C | <ref> }"];
	  d [label="null", shape=plain];
	  header:ref:c -> a [arrowhead=vee, arrowtail=dot, dir=both];
	  a:ref:c -> b:data [arrowhead=vee, arrowtail=dot, dir=both];
	  b:ref:c -> c:data [arrowhead=vee, arrowtail=dot, dir=both];
	  c:ref:c -> d  [arrowhead=vee, arrowtail=dot, dir=both];

	  topo [shape=none];
	  topo -> header [arrows="none", tailclip=true];
  }
#+end_src

#+RESULTS:
[[file:images/pilha_linked.png]]

Aqui, também é possível utilizar um nó sentinela para facilitar
inserção e a remoção de elementos (vide [[file:variacoes_listas_ligadas.org][variações de listas]]).

** Operações

*** Inserção (/push/)

Para a inserção, é necessário apenas, criar um novo nó e atualizar as
referências envolvidas.
#+begin_src C++
  new_node = Node(x); // Cria um node com o elemento a ser inserido.
  new_node->next = topo->next;
  topo->next = new_node;
#+end_src

O custo computacional da inserção é constante, $O(1)$.

*** Remoção (/pop/)

Para a remoção, pode-se proceder de maneira similar à remoção do
primeiro elemento em uma lista encadeada.

#+begin_src C++
  if(not empty()){
      to_delete = topo->next;
      topo->next = topo->next->next;
      delete = to_delete;
  }
#+end_src

A operação =empty()= verifica se a pilha está vazia. Portanto, o custo computacional da remoção é constante, $O(1)$. 

*** Outras operações

 * =top=: Retornar o valor armazenado na célula referente ao topo da
   pilha, $O(1)$.
 * =size=: Basta guardar a informação em um atributo específico, $O(1)$.
 * =empty=: Verificar se =topo->next= está é "vazio", $O(1)$.

Lembrando que para uma implementação em linguagens como C++, é
necessário implementar a liberação de memória de todos os elementos,
no destruturor. Essa operação, tem custo $O(n)$.
 
* Comparação entre as implementações

 * No caso de dificuldade de estimar o número de elementos máximo da
   pilha, a implementação encadeada pode ser mais interessante.
 * A implementação encadeada utiliza mais memória por elemento.

* Aplicações de pilha

 * [[file:sequencias_bem_formadas.org][Sequências bem formadas de parênteses]].
 * [[file:conversao_infixa_posfixa.org][Conversão de notação infixa para pósfixa]].
 * Pilha de execução de um programa.
