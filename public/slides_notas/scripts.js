function toggleCodeBlock(blockId) {
  var x = document.getElementById(blockId);
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
